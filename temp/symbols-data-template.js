__iconsData: {
    
        'i-calendar': {
            width: '512px',
            height: '512px'
        },
    
        'i-calendar_36x36': {
            width: '36px',
            height: '36px'
        },
    
        'i-car_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-charter_65x65': {
            width: '64.67px',
            height: '65.17px'
        },
    
        'i-chart_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-check_45x45': {
            width: '45px',
            height: '45px'
        },
    
        'i-check_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-child_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-clip_50x50': {
            width: '50px',
            height: '50px'
        },
    
        'i-clock_65x65': {
            width: '64.67px',
            height: '65.17px'
        },
    
        'i-cup_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-data1_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-data2_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-del_45x45': {
            width: '45px',
            height: '45px'
        },
    
        'i-del_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-exit_45x45': {
            width: '45px',
            height: '45px'
        },
    
        'i-eye': {
            width: '512px',
            height: '512px'
        },
    
        'i-factory_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-folder_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-head_70x70': {
            width: '52.5px',
            height: '70px'
        },
    
        'i-highway_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-left_30x30': {
            width: '30px',
            height: '30px'
        },
    
        'i-list-check_65x65': {
            width: '64.67px',
            height: '65.17px'
        },
    
        'i-list-check_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-list-check_75x84': {
            width: '75px',
            height: '84px'
        },
    
        'i-medicine_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-moneybox_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-no_45x45': {
            width: '45px',
            height: '45px'
        },
    
        'i-pen-2_45x45': {
            width: '45px',
            height: '45px'
        },
    
        'i-pen_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-right_30x30': {
            width: '30px',
            height: '30px'
        },
    
        'i-scheme_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-search_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-settings_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-settings_65x65': {
            width: '64.67px',
            height: '65.17px'
        },
    
        'i-skyscraper_60x60': {
            width: '60px',
            height: '60px'
        },
    
        'i-users-2_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-users-3_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'i-users_70x70': {
            width: '70px',
            height: '70px'
        },
    
        'language': {
            width: '30px',
            height: '30px'
        },
    
}
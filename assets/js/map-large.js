

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(50.4501, 30.5234);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 11,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"lightness":"40"},{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":"10"}]},{"featureType":"administrative.locality","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":"25"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#243c64"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ff8800"},{"saturation":"19"},{"lightness":"11"},{"gamma":1}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#576377"},{"lightness":"31"},{"saturation":"-30"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"30"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"weight":"1"},{"saturation":"0"},{"lightness":"10"},{"gamma":"1"},{"visibility":"on"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#243c64"},{"lightness":"-32"},{"saturation":"43"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#ff8800"},{"saturation":-61.8},{"lightness":"81"},{"gamma":1}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#ff0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#0080ff"},{"saturation":"-7"},{"lightness":"7"},{"gamma":1}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"30"}]}]
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(50.4501, 30.5234)
  });

  var contentString =
    '<span class="map-popover clearfix" href="#">' +
      '<span class="map-popover__inner">' +
        '<span class="map-popover__title">Територіальний виборчий округ №3</span>' +
        '<span class="map-popover__name" href="#">Кількість виборців : 2500</span>' +
        '<a class="map-popover__name" href="#">Ivanov Ivan Ivanovich</a>' +
        '<a class="map-popover__name" href="#">Sudir Петро Васильович</a>' +
        '<a class="map-popover__name" href="#">Vasylkiv Микола Олексійович</a>' +
      '</span>' +
    '</span>' ;

  infoBubble = new InfoBubble({
    maxWidth: 200,
    content: contentString
  });

  infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);

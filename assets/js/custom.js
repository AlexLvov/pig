/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Datepicker
-Readmore
-Zoom Images
-Search Animation
-Owl carousel

*/



$(document).ready(function() {

  "use strict";



// PRELOADER //

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');

// DATEPICKER //

  if ($('.input-group.date').length > 0) {
    $('.input-group.date').datepicker({
      language: "uk",
      autoclose: true,
      todayHighlight: true
    });
  }


// READMORE //

  if ($('.js-readmore').length > 0) {
    $('.js-readmore').readmore({
      collapsedHeight: 150,
      moreLink: '<a class="b-persona__link" href="#">Розгорнути</a>',
      lessLink: '<a class="b-persona__link" href="#">Згорнути</a>'
    });
  }



// SELECT CUSTOMIZATION

  if ($('.select_box').length > 0) {
    $(".select_box").chosen({
      disable_search_threshold: 10
    });
  }
  $('#a[data-toggle="tab"]').on('shown.bs.tab', function () {
    $('select').chosen({width: '100%'});
  });

// ZOOM IMAGES //

  if ($('.js-zoom-gallery').length > 0) {
      $('.js-zoom-gallery').each(function() { // the containers for all your galleries
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item', // the selector for gallery item
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }


  if ($('.js-zoom-images').length > 0) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }


  if ($('.popup-youtube, .popup-vimeo, .popup-gmaps').length > 0) {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false
    });
  }



// SEARCH ANIMATION //

  $('.search-open, .search-close').on('click', function(e) {
      e.preventDefault();
      $('.header-search').toggleClass('open-search');
      $('.header-search').toggleClass('open');
  });

// OWL CAROUSEL //

  if ($('.owl-carousel').length > 0) {
    $(".owl-carousel").owlCarousel({
      center: true,
      items:1,
      loop:true,
      nav:true,
      dots: false,
      animateOut: 'slideOutDown',
      animateIn: 'flipInX',
      smartSpeed:450,
      autoplay: true,
      responsive:{
          600:{
              items:2
          }
      }
    });
  }



});
